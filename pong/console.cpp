#include "console.h"

void cursorOff()
{
	CONSOLE_CURSOR_INFO cursorInfo;
	cursorInfo.dwSize   = 50;
	cursorInfo.bVisible = FALSE;
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursorInfo);
}

void moveCursorTo(const SHORT& x, const SHORT& y)
{
	COORD coordinate;
	coordinate.X = x;
	coordinate.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coordinate);
}

void changeColor(const WORD& color)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
}