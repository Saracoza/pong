#include "global.h"
#include "paddle.h"
#include "ball.h"


UINT8 dificultyLvl, gameMode;
INT8  tableMatrix[25][103];
BOOL  served;
UINT8 servingCounter, servingPlayer;
UINT8 scorePlayer1;
UINT8 scorePlayer2;

Paddle paddle1;
Paddle paddle2;
Ball   ball;


void initialise()
{
	for (int i = 1; i < 24; ++i) {
		for (int j = 1; j < 100; ++j) {
			tableMatrix[i][j] = 0;
		}
	}
	for (int i = 1; i < 24; ++i) {
		tableMatrix[i][0]   =  1;
		tableMatrix[i][50]  = -2;
		tableMatrix[i][100] =  1;
	}
	for (int j = 1; j < 100; ++j) {
		tableMatrix[0][j]  = -1;
		tableMatrix[24][j] = -1;
	}
	tableMatrix[0][0]    = 41;
	tableMatrix[0][100]  = 42;
	tableMatrix[24][0]   = 43;
	tableMatrix[24][100] = 44;


	paddle1.color = paddle1.selectedColor;
	paddle1.xCoordinate         = 1;
	paddle1.yCoordinate         = 12;
	paddle1.powerup             = FALSE;
	paddle1.powerupCounter      = 5;
	paddle1.powerup_xCoordinate = 1;

	paddle2.color = paddle2.selectedColor;
	paddle2.xCoordinate         = 99;
	paddle2.yCoordinate         = 12;
	paddle2.powerup             = FALSE;
	paddle2.powerupCounter      = 5;
	paddle2.powerup_xCoordinate = 91;


	for (int i = 0; i <= dificultyLvl; ++i) {
		tableMatrix[paddle1.yCoordinate - i][paddle1.xCoordinate] = 21;
		tableMatrix[paddle1.yCoordinate + i][paddle1.xCoordinate] = 21;
		tableMatrix[paddle2.yCoordinate - i][paddle2.xCoordinate] = 22;
		tableMatrix[paddle2.yCoordinate + i][paddle2.xCoordinate] = 22;
	}


	ball.xCoordinate = paddle1.xCoordinate + 1;
	ball.yCoordinate = paddle1.yCoordinate;
	ball.xSpeed = rand() % 3 + 2;
	ball.ySpeed = -1;
	ball.speedMultiplier = 1;
	ball.target_yCoordinate = 12;


	tableMatrix[ball.yCoordinate][ball.xCoordinate] = 3;


	served = FALSE;
	servingCounter = 2;
	servingPlayer = 1;
	scorePlayer1 = 0;
	scorePlayer2 = 0;
}

void draw()
{
	for (int i = 0; i < 25; ++i) {
		for (int j = 0; j < 101; ++j) {
			switch (tableMatrix[i][j]) {
			case -2: {
				changeColor(15);
				printf("%c", ':');
			} break;
			case -1: {
				changeColor(14);
				printf("%c", 205);
			} break;
			case 0: {
				changeColor(15);
				printf("%c", ' ');
			} break;
			case 1: {
				changeColor(14);
				printf("%c", 186);
			} break;
			case 21: {
				changeColor(paddle1.color);
				printf("%c", 219);
			} break;
			case 22: {
				changeColor(paddle2.color);
				printf("%c", 219);
			} break;
			case 3: {
				changeColor(15);
				printf("%c", 254);
			} break;
			case 41: {
				changeColor(14);
				printf("%c", 201);
			} break;
			case 42: {
				changeColor(14);
				printf("%c", 187);
			} break;
			case 43: {
				changeColor(14);
				printf("%c", 200);
			} break;
			case 44: {
				changeColor(14);
				printf("%c", 188);
			} break;
			}
		}
		printf("\n");
	}


	changeColor(15);
	moveCursorTo(47, 25);
	printf("%d  :  %d", scorePlayer1, scorePlayer2);


	changeColor(12);
	moveCursorTo(paddle1.powerup_xCoordinate, 25);
	printf("%c %c %c %c %c", 178, 178, 178, 178, 178);
	if (gameMode == 2) {
		changeColor(12);
		moveCursorTo(paddle2.powerup_xCoordinate, 25);
		printf("%c %c %c %c %c", 178, 178, 178, 178, 178);
	}
}


void incScorePlayer1()
{
	changeColor(15);
	moveCursorTo(48, 25);
	printf("\b%d", ++scorePlayer1);
}

void incScorePlayer2()
{
	changeColor(15);
	moveCursorTo(54, 25);
	printf("\b%d", ++scorePlayer2);
}

void play()
{
	UINT8 msFpsCounter = 4;
	while (scorePlayer1 < 11 && scorePlayer2 < 11) {
		srand(time(NULL));

		if (_kbhit()) {
			switch (_getch()) {
			case 'a': {
				if (paddle1.yCoordinate - 1 - dificultyLvl !=  0) {
					movePaddleUp(paddle1);
					if (!served && servingPlayer == 1) {
						moveBallUp(ball);
					}
				}
			} break;
			case 'z': {
				if (paddle1.yCoordinate + 1 + dificultyLvl != 24) {
					movePaddleDn(paddle1);
					if (!served && servingPlayer == 1) {
						moveBallDn(ball);
					}
				}
			} break;
			case 's': {
				powerUp(paddle1);
			} break;
			case 'k': {
				if (gameMode == 2 && paddle2.yCoordinate - 1 - dificultyLvl !=  0) {
					movePaddleUp(paddle2);
					if (!served && servingPlayer == 2) {
						moveBallUp(ball);
					}
				}
			} break;
			case 'm': {
				if (gameMode == 2 && paddle2.yCoordinate + 1 + dificultyLvl != 24) {
					movePaddleDn(paddle2);
					if (!served && servingPlayer == 2) {
						moveBallDn(ball);
					}
				}
			} break;
			case 'j': {
				if (gameMode == 2) {
					powerUp(paddle2);
				}
			} break;
			case 'v': {
				if (!served) {
					switch (gameMode) {
					case 1: {
						if (servingPlayer == 1) {
							served = TRUE;
							--servingCounter;
						}
					} break;
					case 2: {
						served = TRUE;
						--servingCounter;
					} break;
					}
				}
			} break;
			case ' ': {
				while (_getch() != ' ');
			} break;
			case 't': {
				return;
			} break;
			}
		}

		if (served) {
			if (msFpsCounter) {
				msFpsCounter = msFpsCounter - ball.speedMultiplier;
			}
			else {
				moveBall(ball);
				msFpsCounter = 4;
			}

			BOOL paddleHit = FALSE;
			if (ball.xCoordinate ==  2) {
				for (INT16 i = -dificultyLvl; i <= dificultyLvl; ++i) {
					if (ball.yCoordinate == paddle1.yCoordinate + i) {
						ball.xSpeed = rand() % 3 + 2;
						paddleHit = TRUE;
						break;
					}
				}
				if ((ball.ySpeed > 0 && ball.yCoordinate == paddle1.yCoordinate - dificultyLvl - 1) ||
					(ball.ySpeed < 0 && ball.yCoordinate == paddle1.yCoordinate + dificultyLvl + 1)) {
					ball.xSpeed = rand() % 3 + 2;
					ball.ySpeed = -ball.ySpeed;
					paddleHit = TRUE;
				}

				switch (paddleHit) {
				case TRUE:  {
					if (ball.speedMultiplier == 2) ball.speedMultiplier = 1;
					if (paddle1.powerup) {
						powerDn(paddle1);
						ball.speedMultiplier = 2;
					}
				} break;
				case FALSE: {
					moveBallInNet1(ball);

					incScorePlayer2();

					powerDn(paddle1);
					powerDn(paddle2);

					switch (servingPlayer) {
					case 1: moveBallOnPaddle1(paddle1, ball); break;
					case 2: moveBallOnPaddle2(paddle2, ball); break;
					}

					served = FALSE;
					if (gameMode == 1 && servingPlayer == 2) {
						Sleep(500);
						served = TRUE;
						--servingCounter;
					}
				} break;
				}
			}
			if (ball.xCoordinate == 98) {
				for (INT16 i = -dificultyLvl; i <= dificultyLvl; ++i) {
					if (ball.yCoordinate == paddle2.yCoordinate + i) {
						ball.xSpeed = -(rand() % 3 + 2);
						paddleHit = TRUE;
						break;
					}
				}
				if ((ball.ySpeed > 0 && ball.yCoordinate == paddle2.yCoordinate - dificultyLvl - 1) ||
					(ball.ySpeed < 0 && ball.yCoordinate == paddle2.yCoordinate + dificultyLvl + 1)) {
					ball.xSpeed = -(rand() % 3 + 2);
					ball.ySpeed = -ball.ySpeed;
					paddleHit = TRUE;
				}

				switch (paddleHit) {
				case TRUE:  {
					if (ball.speedMultiplier == 2) ball.speedMultiplier = 1;
					if (paddle2.powerup) {
						powerDn(paddle2);
						ball.speedMultiplier = 2;
					}
				} break;
				case FALSE: {
					moveBallInNet2(ball);

					incScorePlayer1();

					powerDn(paddle1);
					powerDn(paddle2);

					switch (servingPlayer) {
					case 1: moveBallOnPaddle1(paddle1, ball); break;
					case 2: moveBallOnPaddle2(paddle2, ball); break;
					}

					served = FALSE;
					if (gameMode == 1 && servingPlayer == 2) {
						Sleep(500);
						served = TRUE;
						--servingCounter;
					}
				} break;
				}
			}

			if (gameMode == 1) {
				if (ball.xSpeed > 0 && ball.xCoordinate == 50 + (dificultyLvl - 1) * 12) {
					ball.target_xCoordinate = ball.xCoordinate;
					ball.target_yCoordinate = ball.yCoordinate;
					INT8 buff_ySpeed = ball.ySpeed;

					while (ball.target_xCoordinate != 98) {
						ball.target_xCoordinate += ball.xSpeed;
						ball.target_yCoordinate += buff_ySpeed;

						if (ball.target_yCoordinate ==  1 ||
							ball.target_yCoordinate == 23) {
							buff_ySpeed = -buff_ySpeed;
						}
					}
				}

				if (paddle2.yCoordinate > ball.target_yCoordinate &&
					paddle2.yCoordinate - 1 - dificultyLvl !=  0) {
					movePaddleUp(paddle2);
				}
				if (paddle2.yCoordinate < ball.target_yCoordinate &&
					paddle2.yCoordinate + 1 + dificultyLvl != 24) {
					movePaddleDn(paddle2);
				}
			}
		}

		if (servingCounter == 0) {
			switch (servingPlayer) {
			case 1: servingPlayer = 2; break;
			case 2: servingPlayer = 1; break;
			}
			servingCounter = 2;
		}

		Sleep(16);
	}
}


void main()
{
	cursorOff();
	SHORT selectorCoordinate;;
	BOOL  selected;

RESTART:
	changeColor(15);


	printf("PONG\nSelect the difficulty level:\n   hard\n   medium\n   easy");

	moveCursorTo(0, selectorCoordinate = 3);
	printf("\b%c", 219);

	selected = FALSE;
	while (!selected) {
		if (_getch() == 224) {
			switch (_getch()) {
			case 72: {
				if (selectorCoordinate != 2) {
					moveCursorTo(1, selectorCoordinate);
					printf("\b%c", ' ');
					--selectorCoordinate;
					moveCursorTo(1, selectorCoordinate);
					printf("\b%c", 219);
				}
			} break;
			case 80: {
				if (selectorCoordinate != 4) {
					moveCursorTo(1, selectorCoordinate);
					printf("\b%c", ' ');
					++selectorCoordinate;
					moveCursorTo(1, selectorCoordinate);
					printf("\b%c", 219);
				}
			} break;
			case 77: {
				dificultyLvl = selectorCoordinate - 1;
				selected = TRUE;
			} break;
			}
		}
		Sleep(16);
	}


	moveCursorTo(0, 6);
	printf("Select the game mode:\n   1 player\n   2 players");

	moveCursorTo(0, selectorCoordinate = 7);
	printf("\b%c", 219);

	selected = FALSE;
	while (!selected) {
		if (_getch()==224) {
			switch (_getch()) {
			case 72: {
				if (selectorCoordinate != 7) {
					moveCursorTo(1, selectorCoordinate);
					printf("\b%c", ' ');
					--selectorCoordinate;
					moveCursorTo(1, selectorCoordinate);
					printf("\b%c", 219);
				}
			} break;
			case 80: {
				if (selectorCoordinate != 8) {
					moveCursorTo(1, selectorCoordinate);
					printf("\b%c", ' ');
					++selectorCoordinate;
					moveCursorTo(1, selectorCoordinate);
					printf("\b%c", 219);
				}
			} break;
			case 77: {
				gameMode = selectorCoordinate - 6;
				selected = TRUE;
			} break;
			}
		}
		Sleep(16);
	}


	moveCursorTo(0, 10);
	printf("Select the color of the paddle:\n   Player 1:");
	switch (gameMode) {
	case 1: printf("\n   Computer:"); break;
	case 2: printf("\n   Player 2:"); break;
	}
	printf("\n   Start");
	changeColor(paddle1.selectedColor = 9);
	moveCursorTo(17, 11);
	printf("\b%c%c%c%c%c", 254, 254, 254, 254, 254);
	changeColor(paddle2.selectedColor = 9);
	moveCursorTo(17, 12);
	printf("\b%c%c%c%c%c", 254, 254, 254, 254, 254);

	changeColor(15);
	moveCursorTo(0, selectorCoordinate = 11);
	printf("\b%c", 219);

	selected = FALSE;
	while (!selected) {
		if (_getch() == 224) {
			switch (_getch()) {
			case 72: {
				if (selectorCoordinate != 11) {
					changeColor(15);
					moveCursorTo(1, selectorCoordinate);
					printf("\b%c", ' ');
					--selectorCoordinate;
					moveCursorTo(1, selectorCoordinate);
					printf("\b%c", 219);
				}
			} break;
			case 80: {
				if (selectorCoordinate != 13) {
					changeColor(15);
					moveCursorTo(1, selectorCoordinate);
					printf("\b%c", ' ');
					++selectorCoordinate;
					moveCursorTo(1, selectorCoordinate);
					printf("\b%c", 219);
				}
			} break;
			case 75: {
				switch (selectorCoordinate) {
				case 11: {
					if (paddle1.selectedColor != 1) {
						--paddle1.selectedColor;
						changeColor(paddle1.selectedColor);
						moveCursorTo(17, 11);
						printf("\b%c%c%c%c%c", 254, 254, 254, 254, 254);
					}
				} break;
				case 12: {
					if (paddle2.selectedColor != 1) {
						--paddle2.selectedColor;
						changeColor(paddle2.selectedColor);
						moveCursorTo(17, 12);
						printf("\b%c%c%c%c%c", 254, 254, 254, 254, 254);
					}
				}
				}
			} break;
			case 77: {
				switch (selectorCoordinate) {
				case 11: {
					if (paddle1.selectedColor != 11) {
						++paddle1.selectedColor;
						changeColor(paddle1.selectedColor);
						moveCursorTo(17, 11);
						printf("\b%c%c%c%c%c", 254, 254, 254, 254, 254);
					}
				} break;
				case 12: {
					if (paddle2.selectedColor != 11) {
						++paddle2.selectedColor;
						changeColor(paddle2.selectedColor);
						moveCursorTo(17, 12);
						printf("\b%c%c%c%c%c", 254, 254, 254, 254, 254);
					}
				} break;
				case 13: {
					paddle1.color = paddle2.selectedColor;
					paddle2.color = paddle2.selectedColor;
					selected = TRUE;
				} break;
				}
			} break;
			}
		}
		Sleep(16);
	}


	system("cls");
	initialise();
	draw();
	play();


	system("cls");
	changeColor(15);
	if (scorePlayer1 == 11) {
		switch (gameMode) {
		case 1: printf("You won. Congratulations!\n");      break;
		case 2: printf("Player 1 won. Congratulations!\n"); break;
		}
	}
	if (scorePlayer2 == 11) {
		switch (gameMode) {
		case 1: printf("You lost.\n");                      break;
		case 2: printf("Player 1 won. Congratulations!\n"); break;
		}
	}

	printf("To play again, press 'r'.");
	if (_getch() == 'r') {
		system("cls");
		goto RESTART;
	}
}