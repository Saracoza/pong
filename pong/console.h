#pragma once

#include <windows.h>

void changeColor(const WORD& color);
void cursorOff();
void moveCursorTo(const SHORT& x, const SHORT& y);