#pragma once

#include "global.h"

struct Paddle;

struct Ball
{
	SHORT xCoordinate;
	SHORT yCoordinate;
	INT8  xSpeed;
	INT8  ySpeed;
	UINT8 speedMultiplier;
	SHORT target_xCoordinate;
	SHORT target_yCoordinate;
};

void moveBallUp(Ball& ball);
void moveBallDn(Ball& ball);

void moveBall(Ball& ball);

void moveBallInNet1(Ball& ball);
void moveBallInNet2(Ball& ball);

void moveBallOnPaddle1(const Paddle& paddle, Ball& ball);
void moveBallOnPaddle2(const Paddle& paddle, Ball& ball);