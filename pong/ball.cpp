#include "ball.h"
#include "Paddle.h"

void moveBallUp(Ball& ball)
{
	changeColor(15);
	moveCursorTo(ball.xCoordinate + 1, ball.yCoordinate);
	printf("\b%c", ' ');
	--ball.yCoordinate;
	moveCursorTo(ball.xCoordinate + 1, ball.yCoordinate);
	printf("\b%c", 254);
}

void moveBallDn(Ball& ball)
{
	changeColor(15);
	moveCursorTo(ball.xCoordinate + 1, ball.yCoordinate);
	printf("\b%c", ' ');
	++ball.yCoordinate;
	moveCursorTo(ball.xCoordinate + 1, ball.yCoordinate);
	printf("\b%c", 254);
}

void moveBall(Ball& ball)
{
	changeColor(15);

	moveCursorTo(ball.xCoordinate + 1, ball.yCoordinate);
	if (ball.xCoordinate == 50) printf("\b%c", ':');
	else                        printf("\b%c", ' ');

	ball.xCoordinate = ball.xCoordinate + ball.xSpeed;
	ball.yCoordinate = ball.yCoordinate + ball.ySpeed;
	moveCursorTo(ball.xCoordinate + 1, ball.yCoordinate);
	printf("\b%c", 254);

	if (ball.yCoordinate ==  1 ||
		ball.yCoordinate == 23) {
		ball.ySpeed = -ball.ySpeed;
	}
}

void moveBallInNet1(Ball& ball)
{
	changeColor(15);
	moveCursorTo(ball.xCoordinate + 1, ball.yCoordinate);
	printf("\b%c", ' ');
	ball.xCoordinate = ball.xCoordinate - 1;
	ball.yCoordinate = ball.yCoordinate + ball.ySpeed;
	moveCursorTo(ball.xCoordinate + 1, ball.yCoordinate);
	printf("\b%c", 254);
	Sleep(50);
	moveCursorTo(ball.xCoordinate + 1, ball.yCoordinate);
	printf("\b%c", ' ');
}

void moveBallInNet2(Ball& ball)
{
	changeColor(15);
	moveCursorTo(ball.xCoordinate + 1, ball.yCoordinate);
	printf("\b%c", ' ');
	ball.xCoordinate = ball.xCoordinate + 1;
	ball.yCoordinate = ball.yCoordinate + ball.ySpeed;
	moveCursorTo(ball.xCoordinate + 1, ball.yCoordinate);
	printf("\b%c", 254);
	Sleep(50);
	moveCursorTo(ball.xCoordinate + 1, ball.yCoordinate);
	printf("\b%c", ' ');
}

void moveBallOnPaddle1(const Paddle& paddle1, Ball& ball)
{
	changeColor(15);

	ball.xCoordinate = paddle1.xCoordinate + 1;
	ball.yCoordinate = paddle1.yCoordinate;
	ball.xSpeed = rand() % 3 + 2;
	ball.ySpeed = -1;
	ball.speedMultiplier = 1;

	moveCursorTo(ball.xCoordinate + 1, ball.yCoordinate);
	printf("\b%c", 254);
}

void moveBallOnPaddle2(const Paddle& paddle2, Ball& ball)
{
	changeColor(15);

	ball.xCoordinate = paddle2.xCoordinate - 1;
	ball.yCoordinate = paddle2.yCoordinate;
	ball.xSpeed = -(rand() % 3 + 2);
	ball.ySpeed = -1;
	ball.speedMultiplier = 1;

	moveCursorTo(ball.xCoordinate + 1, ball.yCoordinate);
	printf("\b%c", 254);
}