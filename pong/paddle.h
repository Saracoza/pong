#pragma once

#include "global.h"

struct Ball;

struct Paddle
{
	WORD  color;
	WORD  selectedColor;
	SHORT xCoordinate;
	SHORT yCoordinate;
	BOOL  powerup;
	UINT8 powerupCounter;
	SHORT powerup_xCoordinate;
};

void movePaddleUp(Paddle& paddle);
void movePaddleDn(Paddle& paddle);

void powerUp(Paddle& paddle);
void powerDn(Paddle& paddle);