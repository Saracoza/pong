#include "paddle.h"
#include "ball.h"

void movePaddleUp(Paddle& paddle)
{
	changeColor(paddle.color);
	moveCursorTo(paddle.xCoordinate + 1, paddle.yCoordinate + dificultyLvl);
	printf("\b%c", ' ');
	--paddle.yCoordinate;
	moveCursorTo(paddle.xCoordinate + 1, paddle.yCoordinate - dificultyLvl);
	printf("\b%c", 219);
}

void movePaddleDn(Paddle& paddle)
{
	changeColor(paddle.color);
	moveCursorTo(paddle.xCoordinate + 1, paddle.yCoordinate - dificultyLvl);
	printf("\b%c", ' ');
	++paddle.yCoordinate;
	moveCursorTo(paddle.xCoordinate + 1, paddle.yCoordinate + dificultyLvl);
	printf("\b%c", 219);
}

void powerUp(Paddle& paddle)
{
	if (served && !paddle.powerup && paddle.powerupCounter) {
		changeColor(paddle.color = 12);

		for (INT16 i = -dificultyLvl; i <= dificultyLvl; ++i) {
			moveCursorTo(paddle.xCoordinate + 1, paddle.yCoordinate + i);
			printf("\b%c", 219);
		}
		moveCursorTo(paddle.powerup_xCoordinate - 1 + (2 * paddle.powerupCounter), 25);
		printf("\b%c", ' ');

		--paddle.powerupCounter;
		paddle.powerup = TRUE;
	}
}

void powerDn(Paddle& paddle)
{
	if (paddle.powerup) {
		changeColor(paddle.color = paddle.selectedColor);

		for (INT16 i = -dificultyLvl; i <= dificultyLvl; ++i) {
			moveCursorTo(paddle.xCoordinate + 1, paddle.yCoordinate + i);
			printf("\b%c", 219);
		}

		paddle.powerup = FALSE;
	}
}