#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>

#include "console.h"

extern UINT8 dificultyLvl, gameMode;
extern INT8  tableMatrix[25][103];
extern BOOL  served;
extern UINT8 servingCounter, servingPlayer;
extern UINT8 scorePlayer1;
extern UINT8 scorePlayer2;